# Gitea Theme Updater

This is a simple class that lets you use Gitea releases in your repository to handle your theme updates. As Aristeides Stathopoulos, the author of the code we forked to make this, once said, "A more robust solution would be to use the [github-updater](https://github.com/afragen/github-updater) plugin, but I simply refuse to include a 540kb library in my theme when I can accomplish what I need in less than 200 lines of code.<br> 
Is it perfect? No. Does it get the job done? Yes."

## Implementing in the theme:

1. Download the php file from this repo and include it in your theme, or require it using composer:
	```bash
	composer require sandiamesa/gitea-theme-updater
	```
2. If you're using a namespace, the namespace on the top of the file to match the one you use in your theme. Otherwise, remove the namespace on the top of the file.

3. Replace `code.sandiamesa.com` with the Gitea instance where you host your WordPress theme.
4. Include the updater class in your functions.php file:
	```
	add_action( 'after_setup_theme', function() {
		get_template_part( 'inc/classes/Updater' );
	});
	```
5. At the bottom of the `Updater.php` file init the updater:
	```
    if ( is_admin() ) {
	new Updater(
            [
                'name' => 'Gridd',                     // Theme Name.
                'repo' => 'wplemon/gridd',             // Theme repository.
                'slug' => 'gridd',                     // Theme Slug.
                'url'  => 'https://wplemon.com/gridd', // Theme URL.
                'ver'  => '1.2'                        // Theme Version.
            ]
        );
    }
	```
**NOTE:** If you are using the WordPress OpenID plugin along with a theme that includes this updater, just add the Updater class to the functions.php file of your theme. It will prevent login via OpenID conflicts.

## Releasing an update on Gitea

When you want to release an update you can simply go to your repository > Releases and create a new release.  
* The version should either be formatted as `1.2`, `1.2.3`, `v1.2` or `v1.2.3` etc. The version comparison will remove the `v` and the script will be able to compare versions. However, if you call your release-tag `tomato` don't expect it to work.
* You **have to upload a zip file.**. When creating your release, upload a `.zip` file in there. The default packages created by Gitea have the version number inside the folder name, and that has the potential to break things on user sites when updating. **This updater script will only detect the file uploaded as an asset manually by you**.

## Advice

* Use at your own risk. If you don't follow the instructions above, things will break.
* This script is as simple as it can be. If there are any bugs, please let us know by submitting an issue to our Gitea Issues tracker.

## Why was this created?

This project was created out of necessity for a theme submitted on wordpress.org. The theme review process on wordpress.org takes a long time...

* The initial review is usually a couple of months.
* If all goes well the theme proceeds to the final review which takes 2-4 months at the time of this writing.
* If your theme is tagged as accessibility-ready, then the theme has to wait for a 3rd review, and I currently have no idea how long that will take.

Overall a theme review can easily take more than 6 months.

In the meantime, life goes on. If your business depends on your themes, then you may want to implement an alternative method to provide updates for your theme so people can start using it and you can start growing.

## Releasing your theme on w.org

Remember to remove the script when uploading an update on w.org. When your theme goes live you can remove it from the repository as well, this is only meant as a stepping stone.

Until your theme goes live you can ignore the class inclusion if you did it using `get_template_part` as in the example above. If the file doesn't exist nothing bad will happen.

Stathopoulos prefers using a `.gitattributes` file to exclude the development files like `.sass`, `.map`, `.editorconfig` etc. If you use such a file on your project, you can just add `Updater.php export-ignore` in there and it won't be included in your theme export when you get your build ready for w.org

That's all. Aristeides Stathopoulos and Sandia Mesa hope you enjoy using this and it makes your life somewhat easier.